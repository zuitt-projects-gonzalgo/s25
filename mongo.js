//Aggregation in MongoDB

//mini-activity

db.fruits.insertMany([
{
	"name" : "Apple",
	"supplier" : "Red Farms Inc.",
	"stocks" : 20,
	"price" : 40,
	"onSale" :true
},

{
	"name" : "Banana",
	"supplier" : "Yellow Farms",
	"stocks" : 15,
	"price" : 20,
	"onSale" :true
},

{
	"name" : "Kiwi",
	"supplier" : "Green Farming and Canning",
	"stocks" : 25,
	"price" : 50,
	"onSale" :true
},

{
	"name" : "Mango",
	"supplier" : "Yellow Farms",
	"stocks" : 10,
	"price" : 60,
	"onSale" :true
},

{
	"name" : "Dragon Fruit",
	"supplier" : "Red Farms Inc.",
	"stocks" : 10,
	"price" : 60,
	"onSale" :true
}
])


//aggregation in MongoDB
	//this is the act of process of generating  manipulated data and perform operations to create filtered results that helps in data analysis
	//this helps in creating reports from analyzing the data provided in our documents
	//this helps in arriving at summarized information NOT previously shown in our documents

	//sggregation pipelines
		//aggregation is done in 2 to 3 steps. Each stage is called a pipeline
		//the first pipeline in our example is with the use of $match
		//$match is used to pass the document or get the documents that will pass our condition
			//syntax: {$match : {field : value}}
		//the second pipeline in our example is with the use of $group
		//$group grouped elements/ documents together and create an analysis of these grouped documents
			//syntax: {$group : {_id : <id>, fieldResult : "valueResult"}}
			//_id: this is us associating an id for aggregated result
				//when id is provided with $supplier, we created results per supplier
				//$field will refer to a field name that is available in the documents that being aggregate on
			//$sum will get the total of all the values of the given field. This will allows us to get the total of the values of the given field per group. It will non-numerical values


db.fruits.aggregate([
	{$match : {"onSale" : true}},
	{$group : {_id : "$supplier", totalStocks : {$sum : "$stocks"}}}
])

db.fruits.aggregate([
	{$match : {onSale : true}},
	{$group : {_id : '$supplier', totalPrice : {$sum : '$price'}}}
])


//mini-activity
db.fruits.aggregate([
	{$match : {supplier : 'Red Farms Inc.'}},
	{$group : {_id : 'For Sale $s', totalStocks : {$sum : '$stocks'}}}

])

//average price of all items on sale

db.fruits.aggregate([
	{$match : {onSale : true}},
	{$group : {_id : 'Average Price on Sale', avgPrice : {$avg : '$price'}}}
])

//$max allow us to get the highest value out of all the values of a given field
db.fruits.aggregate([
	{$match : {onSale : true}},
	{$group : {_id : "$name", maxStock : {$max : '$stocks'}}}
])

//mini-activity
db.fruits.aggregate([
	{$match : {onSale :false}},
	{$count : 'Not for Sale'}
])

db.fruits.aggregate([
	{$match : {onSale : true}},
	{$group : {_id : "$supplier", Number_of_Fruits_Supplied : {$count : {}}}}
])


//

db.fruits.aggregate([
	{$match : {onSale : true}},
	{$group : {_id : '$supplier', totalStocks : {$sum : '$stocks'}}},
	{$out : 'Total Stocks Per Supplier'}
])